# Capslock Remapping Script

Danik at danikgames.com

## Functionality

- Deactivates capslock for normal (accidental) use.
- Hold Capslock and drag anywhere in a window to move it (not just the title bar).
- Access the following functions when pressing Capslock:
  - Cursor keys           - J, K, L, I
  - Space                 - Enter
  - Home, PgDn, PgUp, End - U, O, Y, H
  - Backspace and Del     - N, M
  - Insert                - B
  - Select all            - A
  - Cut, copy, paste      - S, D, F
  - Close tab, window     - W, E
  - Esc                   - R
  - Next, previous tab    - Tab, Q
  - Undo, redo            - , and .

To use capslock as you normally would, you can press `WinKey` + `Capslock`

![Keyboard Layout](./CapsLock-binding-keyboard-layout-ALT-small-space.png)
